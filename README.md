# ES Music Bot for Discord


```
npm install
```

# Setup bota
#### Vytvořit aplikaci -> vytvořit bota -> získat api klíč
##### https://discord.com/developers/applications

#### Vložit klíč z discord developer portal sem
```
BOT_TOKEN=<your token>
```

# Start bota na localu
```
npm run watch
```