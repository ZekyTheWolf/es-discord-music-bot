import { Player } from "discord-player";
import { Client } from "discord.js";

export interface BotInterface {
    Client: Client;
    Player: Player;
}