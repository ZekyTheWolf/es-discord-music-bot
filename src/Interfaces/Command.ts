import { CommandInteraction } from "discord.js";
import {
    SlashCommandBuilder,
    SlashCommandSubcommandsOnlyBuilder,
} from "@discordjs/builders";

import { Permissions } from "../Utils";
  
export interface CommandInterface {
    name: string,
    category: CommandCategory,
    permissions: Permissions,
    data:
        | Omit<SlashCommandBuilder, "addSubcommandGroup" | "addSubcommand">
        | SlashCommandSubcommandsOnlyBuilder;
    run: (interaction: CommandInteraction) => Promise<void>;
}

export class CommandCategory{
    public static get Moderation(): string{
        return "Moderation";
    }

    public static get Fun(): string{
        return "Fun";
    }

    public static get Music(): string{
        return "Music";
    }

    public static get Utility(): string{
        return "Utility";
    }

    public static get Test(): string{
        return "Test";
    }

    public static get Dev(): string{
        return "Dev";
    }

    public static get Info(): string{
        return 'Info';
    }
}
  