import { CommandInterface } from "../Interfaces";

import * as Info from "./Info";
import * as Music from "./Music";

export const CommandList: CommandInterface[] = [
    //Info
    Info.Info,
    
    // Fun

    // Music
    Music.Play, Music.Back, Music.BassBoost, Music.MusicClear, 
    Music.Pause, Music.PlayNext, Music.CurrentQueue, Music.Resume,
    Music.Skip, Music.Stop
];