import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

import { MusicPlayer } from "../..";

export const Skip: CommandInterface = {
    name: "Skip",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("skip")
        .setDescription("Skip to next track."),

    run: async (interaction) => {
        await interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        if (!Queue || !Queue.playing){

            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }


        const success = Queue.skip();
        const currentTrack = Queue.current;

        const msg = MakeEmbed({
            title: success ? `:play_pause: Music | Skipped` : `:x: Music | Error!`,
            color: success ? `#00ff00` : `#ff0000`,
            description: success ? `Skipped to ${currentTrack.title}.` : "Error."
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
