import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { Permissions, MakeEmbed } from "../../Utils";

import { MusicPlayer } from "../..";


export const Back: CommandInterface = {
    name: "Back",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("back")
        .setDescription("Start playing previouse song."),
        
    run: async (interaction) => {
        await interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        if (!Queue || !Queue.playing){
            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            return void interaction.followUp({ embeds: [msg] });
        }

        await Queue.back();

        const msg = MakeEmbed({
            title: `:white_check_mark: Music | Back!`,
            color: `#00ff00`,
            description: `Now playing ${Queue.current.title}`
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
