import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

import { MusicPlayer } from "../..";

export const BassBoost: CommandInterface = {
    name: "bassboost",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("bassboost")
        .setDescription("Toggle bass boost filter."),

    run: async (interaction) => {
        await interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        if (!Queue || !Queue.playing){

            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            return void interaction.followUp({ embeds: [msg] });
        }

        await Queue.setFilters({
            bassboost: !Queue.getFiltersEnabled().includes('bassboost'),
            normalizer2: !Queue.getFiltersEnabled().includes('bassboost') // because we need to toggle it with bass
        });

        const msg = MakeEmbed({
            title: `:white_check_mark: Music | Bass boost`,
            color: `#00ff00`,
            description: `Bass boost filter is now ${Queue.getFiltersEnabled().includes('bassboost') ? 'enabled' : 'disabled'}`
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
