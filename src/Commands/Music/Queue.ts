import { MessageEmbed } from "discord.js";
import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

import { MusicPlayer } from "../..";

export const CurrentQueue: CommandInterface = {
    name: "Queue",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("queue")
        .setDescription("Check in for the 100 Days of Code challenge.")
        .addIntegerOption(option =>
            option.setName('page')
                .setDescription('Specific page of the queue.')
                .setRequired(false)),

    run: async (interaction) => {
        await interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        if (!Queue || !Queue.playing){

            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }

        const page = interaction.options.getInteger('page') || 1;
        const pageStart = 10 * (page - 1);
        const pageEnd = pageStart + 10;
        const currentTrack = Queue.current;
        const tracks = Queue.tracks.slice(pageStart, pageEnd).map((m, i) => {
            return `${i + pageStart + 1}. **${m.title}** ([link](${m.url}))`;
        });

        const msg = new MessageEmbed();
        msg.setColor("#0099ff");
        msg.setTitle(`:play_pause: Music | Queue`);
        msg.setDescription(tracks.join("\n"));
        msg.addField("Current Track", `**${currentTrack.title}** ([link](${currentTrack.url}))`);
        msg.setFooter({ text: `© 2022 by ${process.env.BOT_AUTHOR}` });

        await interaction.editReply({ embeds: [ msg ] });
    }
};
