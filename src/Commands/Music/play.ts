import { SlashCommandBuilder } from "@discordjs/builders";
import { Guild, GuildMember, VoiceChannel } from "discord.js";
import { QueryType } from "discord-player";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { Permissions, MakeEmbed } from "../../Utils";

import { MusicPlayer } from "../..";

export const Play: CommandInterface = {
    name: "Play",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("play")
        .setDescription("Play music from youtube.")
        .addStringOption(option =>
            option.setName('track')
                .setDescription('Url to music/Name music')
                .setRequired(true)),

    run: async (interaction) => {
        await interaction.deferReply();

        const member = interaction.member as GuildMember;
        const songTitle = interaction.options.getString("track") as string;

        if(!member.voice.channel)
            return void interaction.followUp({
                content: "Please join a voice channel first!",
            });

        
        const searchResult = await MusicPlayer.search(songTitle, {
            requestedBy: interaction.user,
            searchEngine: QueryType.AUTO,
        });

        if (!searchResult || !searchResult.tracks.length){
            const msg = MakeEmbed({
                title: `:x: Music | No results found!`,
                color: `#ff0000`,
                description: `No results found for ${songTitle}.`
            });

            return void interaction.editReply({ embeds: [msg] });
        }


        const Queue = MusicPlayer.createQueue(interaction.guild as Guild, {
            ytdlOptions: {
                filter: 'audioonly',
                highWaterMark: 1 << 30,
                dlChunkSize: 0,
            },
            metadata: interaction.channel,
        });

        try{
            if (!Queue.connection)
                await Queue.connect(member.voice.channel as VoiceChannel);
        } catch {
            void MusicPlayer.deleteQueue(interaction.guild as Guild);

            const msg = MakeEmbed({
                title: `:x: Music | Error!`,
                color: `#ff0000`,
                description: `Sorry, im unable to join channel.`
            });

            return void interaction.editReply({ embeds: [ msg ] });
        }

        if(searchResult.playlist){
            const msg = MakeEmbed({
                title: ":play_pause: Music | Playlist found!",
                color: "GREEN",
                description: `Playlist : ${searchResult.playlist.title} found!`
            });

            Queue.addTracks(searchResult.tracks);

            await interaction.editReply({ embeds: [ msg ] });
        }else{
            const msg = MakeEmbed({
                title: ":play_pause: Music | Track found!",
                color: "GREEN",
                description: `Track : ${searchResult.tracks[0].title} added!`
            });

            Queue.addTrack(searchResult.tracks[0]);
            await interaction.editReply({ embeds: [ msg ] });
        }
        
        if (!Queue.playing){
            const msg = MakeEmbed({
                title: ":play_pause: Music | Playing!",
                color: "GREEN",
                description: `Playing : ${searchResult.tracks[0].title}`,
                thumbnail: searchResult.tracks[0].thumbnail
            });
            await Queue.play();
            await interaction.editReply({ embeds: [ msg ] });
        }
    }
};
