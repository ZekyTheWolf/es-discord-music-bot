import { SlashCommandBuilder } from "@discordjs/builders";
import { QueryType } from "discord-player";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

import { MusicPlayer } from "../..";

export const PlayNext: CommandInterface = {
    name: "Next",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("playnext")
        .setDescription("Track what you want play next.")
        .addStringOption(option =>
            option.setName('track')
                .setDescription('Url to music/Name music')
                .setRequired(true)),

    run: async (interaction) => {
        interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        const Track = interaction.options.getString('track') as string;

        if (!Queue || !Queue.playing){

            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }

        const searchResult = await MusicPlayer
            .search(Track, {
                requestedBy: interaction.user,
                searchEngine: QueryType.AUTO
            })
            .catch(() => {
                console.log('error search');
            });

        if (!searchResult) {
            const msg = MakeEmbed({
                title: ":x: Music | Error!",
                color: "#ff0000",
                description: `Something went wrong, check console.`
            });
            return void interaction.editReply({ embeds: [msg] });
        }

        Queue.insert(searchResult.tracks[0]);

        const msg = MakeEmbed({
            title: ":play_pause: Music",
            color: "GREEN",
            description: `Next track will be ${searchResult.tracks[0].title}`
        });

        await interaction.editReply({ embeds: [msg] });
    }
};


