import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

import { MusicPlayer } from "../..";

export const MusicClear: CommandInterface = {
    name: "Clear",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("queue-clear")
        .setDescription("Clear current queue."),

    run: async (interaction) => {
        await interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        if (!Queue || !Queue.playing){

            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }

        Queue.clear();

        const msg = MakeEmbed({
            title: `:white_check_mark: Music | Queue cleared!`,
            color: `#00ff00`,
            description: `Queue cleared.`
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
