import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory  } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

import { MusicPlayer } from "../..";

export const Pause: CommandInterface = {
    name: "Pause",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,
    
    data: new SlashCommandBuilder()
        .setName("pause")
        .setDescription("Pause current track."),

    run: async (interaction) => {
        await interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        if (!Queue || !Queue.playing){

            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }

        const paused = Queue.setPaused(true);

        const msg = MakeEmbed({
            title: paused ? `:pause_button: Music | Paused!` : `:x: Music | Error!`,
            color: paused ? `#00ff00` : `#ff0000`,
            description: paused ? "Paused." : "Error."
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
