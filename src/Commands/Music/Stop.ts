import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

import { MusicPlayer } from "../..";

export const Stop: CommandInterface = {
    name: "Stop",
    category: CommandCategory.Music,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("stop")
        .setDescription("Stop current queue."),

    run: async (interaction) => {
        await interaction.deferReply();

        const Queue = MusicPlayer.getQueue(interaction.guildId as string);
        if (!Queue || !Queue.playing){

            const msg = MakeEmbed({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }

        Queue.destroy();

        const msg = MakeEmbed({
            title: `:play_pause: Music | Success`,
            color: `#00ff00`,
            description: "Queue stopped."
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
