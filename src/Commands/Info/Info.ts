import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory  } from "../../Interfaces";
import { MakeEmbed, Permissions } from "../../Utils";

export const Info: CommandInterface = {
    name: "Info Command",
    category: CommandCategory.Info,
    permissions: Permissions.NONE,

    data: new SlashCommandBuilder()
        .setName("info")
        .setDescription("Info about server."),
    run: async (interaction) => {
        await interaction.deferReply();

        const msg = MakeEmbed({
            title: `Bot info`,
            color: `#FF0000`,
            description: `\nBot is replacment for serveral bots, it can be assumend as All In One.`,
            fields: [
                { name: `Author`, value: `Emperor Studio [ES]`, inline: true },
                { name: `Version`, value: `${process.env.BOT_VERSION}`, inline: true },
                { name: `Status`, value: `Development`, inline: true }
            ]
        });

        await interaction.editReply({ embeds: [ msg ] });
    }
};
