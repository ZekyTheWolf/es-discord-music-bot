import { SlashCommandBuilder } from "@discordjs/builders";

import { CommandInterface, CommandCategory  } from "../Interfaces";
import { Permissions } from "../Utils";

export const Test: CommandInterface = {
    name: "Test Command",
    category: CommandCategory.Test,
    permissions: Permissions.ADMINISTRATOR,
    data: new SlashCommandBuilder()
        .setName("test")
        .setDescription("Check in for the 100 Days of Code challenge."),
    run: async (interaction) => {
        interaction.reply("Hello World!");
    }
};
