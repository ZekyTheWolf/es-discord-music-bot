import { Client } from "discord.js";
import { Player } from "discord-player";

import * as Events from "./Events";
import * as Interface from "./Interfaces";

const BOT = new Client({
    intents: 641
});

const MusicPlayer = new Player(BOT);
    
async function startBot() {
    BOT.on(
        "interactionCreate",
        async (interaction) => await Events.onInteraction(interaction)
    );
    
    BOT.on(
        "messageCreate",
        async (message) => await Events.onMessage(message)
    )
        
    BOT.on(
        "ready", 
        async () => await Events.onReady(BOT)
    );
    
    MusicPlayer.on(
        "trackStart",
        async (queue, track) => await Events.onTrackStart(queue as Interface.QueueInterface, track)
    );
    MusicPlayer.on(
        "trackEnd",
        async (queue, track) => await Events.onTrackEnd(queue as Interface.QueueInterface, track)
    );
    

    BOT.login(process.env.BOT_TOKEN);
};

export { 
    startBot,
    BOT, 
    MusicPlayer
}
