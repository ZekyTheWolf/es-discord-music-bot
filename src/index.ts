import { startBot, BOT, MusicPlayer } from "./Bot";

startBot();

export {
    MusicPlayer,
    BOT,
};