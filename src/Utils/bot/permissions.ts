import { PermissionString } from "discord.js";

export class Permissions{
    public static get CREATE_INSTANT_INVITE(): PermissionString{
        return "CREATE_INSTANT_INVITE";
    }

    public static get KICK_MEMBERS(): PermissionString{
        return "KICK_MEMBERS";
    }

    public static get BAN_MEMBERS(): PermissionString{
        return "BAN_MEMBERS";
    }

    public static get ADMINISTRATOR(): PermissionString{
        return "ADMINISTRATOR";
    }

    public static get MANAGE_CHANNELS(): PermissionString{
        return "MANAGE_CHANNELS";
    }

    public static get MANAGE_GUILD(): PermissionString{
        return "MANAGE_GUILD";
    }

    public static get ADD_REACTIONS(): PermissionString{
        return "ADD_REACTIONS";
    }

    public static get VIEW_AUDIT_LOG(): PermissionString{
        return "VIEW_AUDIT_LOG";
    }

    public static get VIEW_CHANNEL(): PermissionString{
        return "VIEW_CHANNEL";
    }

    public static get SEND_MESSAGES(): PermissionString{
        return "SEND_MESSAGES";
    }

    public static get SEND_TTS_MESSAGES(): PermissionString{
        return "SEND_TTS_MESSAGES";
    }

    public static get MANAGE_MESSAGES(): PermissionString{
        return "MANAGE_MESSAGES";
    }

    public static get EMBED_LINKS(): PermissionString{
        return "EMBED_LINKS";
    }

    public static get ATTACH_FILES(): PermissionString{
        return "ATTACH_FILES";
    }

    public static get READ_MESSAGE_HISTORY(): PermissionString{
        return "READ_MESSAGE_HISTORY";
    }
    
    public static get MENTION_EVERYONE(): PermissionString{
        return "MENTION_EVERYONE";
    }

    public static get USE_EXTERNAL_EMOJIS(): PermissionString{
        return "USE_EXTERNAL_EMOJIS";
    }

    public static get CONNECT(): PermissionString{
        return "CONNECT";
    }

    public static get SPEAK(): PermissionString{
        return "SPEAK";
    }

    public static get MUTE_MEMBERS(): PermissionString{
        return "MUTE_MEMBERS";
    }

    public static get DEAFEN_MEMBERS(): PermissionString{
        return "DEAFEN_MEMBERS";
    }

    public static get MOVE_MEMBERS(): PermissionString{
        return "MOVE_MEMBERS";
    }

    public static get USE_VAD(): PermissionString{
        return "USE_VAD";
    }

    public static get CHANGE_NICKNAME(): PermissionString{
        return "CHANGE_NICKNAME";
    }

    public static get MANAGE_NICKNAMES(): PermissionString{
        return "MANAGE_NICKNAMES";
    }

    public static get MANAGE_ROLES(): PermissionString{
        return "MANAGE_ROLES";
    }

    public static get MANAGE_WEBHOOKS(): PermissionString{
        return "MANAGE_WEBHOOKS";
    }

    public static get MANAGE_EMOJIS_AND_STICKERS(): PermissionString{
        return "MANAGE_EMOJIS_AND_STICKERS";
    }

    public static get USE_APPLICATION_COMMANDS(): PermissionString{
        return "USE_APPLICATION_COMMANDS";
    }

    public static get REQUEST_TO_SPEAK(): PermissionString{
        return "REQUEST_TO_SPEAK";
    }

    public static get MANAGE_THREADS(): PermissionString{
        return "MANAGE_THREADS";
    }

    public static get USE_PUBLIC_THREADS(): PermissionString{
        return "USE_PUBLIC_THREADS";
    }

    public static get CREATE_PUBLIC_THREADS(): PermissionString{
        return "CREATE_PUBLIC_THREADS";
    }

    public static get CREATE_PRIVATE_THREADS(): PermissionString{
        return "CREATE_PRIVATE_THREADS";
    }

    public static get USE_EXTERNAL_STICKERS(): PermissionString{
        return "USE_EXTERNAL_STICKERS";
    }

    public static get SEND_MESSAGES_IN_THREADS(): PermissionString{
        return "SEND_MESSAGES_IN_THREADS";
    }

    public static get START_EMBEDDED_ACTIVITIES(): PermissionString{
        return "START_EMBEDDED_ACTIVITIES";
    }

    public static get MODERATE_MEMBERS(): PermissionString{
        return "MODERATE_MEMBERS";
    }

    public static get MANAGE_EVENTS(): PermissionString{
        return "MANAGE_EVENTS";
    }

    public static get NONE(): string{
        return "NONE";
    }
}
