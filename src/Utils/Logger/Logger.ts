import clc from "cli-color";
import * as log4js from "log4js";

// Logger(user, danger, `Cant update user ${this.user.id}`);
export const Logger = (
    part: LoggerPart, 
    type: LoggerType, 
    message: string, 
    file: boolean = false, 
    fileName: string = ""
) => {
    let loggerPart: string;
    switch(part) {
        case LoggerPart.database: { loggerPart = "Database"; break; };
        case LoggerPart.user: { loggerPart = "User"; break; };
        case LoggerPart.levelSystem: { loggerPart = "LevelSystem"; break; };
        case LoggerPart.commands: { loggerPart = "Commands"; break; };
    }

    let loggerType: any;
    switch(type) {
        case LoggerType.success: { loggerType = clc.greenBright; break; };
        case LoggerType.danger: { loggerType = clc.redBright; break; };
        case LoggerType.warning: { loggerType = clc.yellowBright; break; };
        case LoggerType.info: { loggerType = clc.cyanBright; break; };
    }

    console.log(loggerType(`| ------------------------------------- |`));
    console.log(loggerType(`| -> ${loggerType(loggerPart)}`));
    console.log(loggerType(`| -> ${clc.white(message)}`));
    console.log(loggerType(`| ------------------------------------- |`));

    if(file) {
        log4js.configure({
            appenders: { cheese: { type: "file", filename: fileName ? fileName : "logs/log.log" } },
            categories: { default: { appenders: ["ES"], level: "error" } }
        });

        const logger = log4js.getLogger("ES");
        switch(type) {
            case LoggerType.success: { logger.info(`${message}`); break; };
            case LoggerType.danger: { logger.error(`${message}`); break; };
            case LoggerType.warning: { logger.warn(`${message}`); break; };
            case LoggerType.info: { logger.debug(`${message}`); break; };
        }
    }
}

export enum LoggerPart { database, user, levelSystem, commands }
export enum LoggerType { success, warning, danger, info }