import { MessageEmbed, ColorResolvable, EmbedFieldData } from "discord.js";

export function MakeEmbed(embed: EmbedOptions) {
    let msg = new MessageEmbed();

    msg.setColor(embed.color);
    msg.setTitle(embed.title);
    msg.setDescription(embed.description);
    if(embed.thumbnail)
        msg.setThumbnail(embed.thumbnail);
    if(embed.fields)
        msg.addFields(embed.fields);
    msg.setFooter({ text: `© 2022 by ${process.env.BOT_AUTHOR}` });

    return msg;
}

interface EmbedOptions {
    title: string;
    color: ColorResolvable;
    description: string;
    thumbnail?: string;
    fields?: EmbedFieldData[];
}
    