import { Client } from "discord.js";
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";

import clc from "cli-color";

import { CommandList } from "../Commands/_CommandList";

export const onReady = async (BOT: Client) => {
    const rest = new REST({ version: "9" }).setToken(
        process.env.BOT_TOKEN as string
    );

    const commandData = CommandList.map((command) => command.data.toJSON());

    await rest.put(
        Routes.applicationCommands(BOT.user?.id || "missing id"),
        { body: commandData }
    );
    

    console.log(clc.yellow(`| -------------------------------------`));
    console.log(clc.yellow(`| ${clc.green(`Starting EmperorBOT core!`)}`));
    console.log(clc.yellow(`| ${clc.green(`Logged as: ${BOT.user?.tag}`)}`));
};
