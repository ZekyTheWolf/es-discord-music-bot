import { TextChannel, MessageActionRow, MessageButton } from "discord.js";
import { Track } from "discord-player";

import { MakeEmbed } from "../../Utils";
import { QueueInterface } from "../../Interfaces";

export const onTrackStart = async (Queue: QueueInterface, track: Track) => {
    const channel = Queue.metadata as TextChannel;
    
    if(Queue.npmessage){
        Queue.npmessage.delete().catch(() => {});
    }
    let row = new MessageActionRow()
        .addComponents(
            new MessageButton()
                .setCustomId('buttoncontrol_play')
                .setLabel('Pause')
                .setStyle('SUCCESS'),
            new MessageButton()
                .setCustomId('buttoncontrol_skip')
                .setLabel('Skip')
                .setStyle('PRIMARY'),
            new MessageButton()
                .setCustomId('buttoncontrol_disconnect')
                .setLabel('Disconnect')
                .setStyle('DANGER'),
            new MessageButton()
                .setCustomId('buttoncontrol_queue')
                .setLabel('Show queue')
                .setStyle('SECONDARY')
        );
    
    const msgEmbed = MakeEmbed({
        title: `:play_pause: Music | Now playing`,
        color: `#00ff00`,
        description: `Now playing ${track.title}.`,
        thumbnail: `${track.thumbnail}`,
    });

    channel.send({ 
        embeds: [ msgEmbed ], 
        components: [ row ] 
    }).then((msg) => {
        Queue.npmessage = msg;
    })
}
