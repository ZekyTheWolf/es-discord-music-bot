import { Track } from "discord-player";

import { QueueInterface } from "../../Interfaces";

export const onTrackEnd = async (Queue: QueueInterface, track: Track) => {
    
    if(Queue.npmessage){
        Queue.npmessage.delete().catch(() => {});
    }
}