import { CommandInteraction, Guild, GuildMember, Interaction, MessageActionRow, MessageButton, MessageEmbed, PermissionResolvable, TextChannel, VoiceChannel } from "discord.js";

import { CommandList } from "../Commands/_CommandList";
import { Permissions, MakeEmbed } from "../Utils";

import { MusicPlayer } from "..";

export const onInteraction = async (interaction: Interaction) => {

    const guild = interaction.guild as Guild;
    // Slash commands
    if (interaction.isCommand()) {

        if(interaction.member?.user.bot) return;
        
        for (const Command of CommandList) {
            if (interaction.commandName === Command.data.name) {
                if(Command.permissions == Permissions.NONE){
                    await Command.run(interaction);
                }else{
                    if(interaction.memberPermissions?.has(Command.permissions as PermissionResolvable)){
                        await Command.run(interaction);
                    } else {
                        const msg = MakeEmbed({
                            title: ":x: | You don't have permission to use this command!",
                            color: "#ff0000",
                            description: `You need the permission \`${Command.permissions}\` to use this command.`
                        });
                        interaction.reply({ embeds: [msg] });
                    }
                }
            }
        }
    }

    // Music Player
    if(interaction.isButton()){
        const Queue = MusicPlayer.getQueue(guild.id);
        const isPaused = Queue.connection.paused;
        const channel = Queue.metadata as TextChannel;

        const member = interaction.member as GuildMember;
        const voiceChannel = member.voice.channel as VoiceChannel
        if(!Queue 
            || !Queue.playing 
            || !voiceChannel.id
            || (guild.me?.voice.channelId && member.voice.channelId !== guild.me?.voice.channelId ))
            return;

        switch(interaction.customId){
            case 'buttoncontrol_play': {
                let row = new MessageActionRow()
                .addComponents(
                    new MessageButton()
                        .setCustomId('buttoncontrol_play')
                        .setLabel(isPaused ? 'Pause' : 'Resume')
                        .setStyle('SUCCESS'),
                    new MessageButton()
                        .setCustomId('buttoncontrol_skip')
                        .setLabel('Skip')
                        .setStyle('PRIMARY'),
                    new MessageButton()
                        .setCustomId('buttoncontrol_disconnect')
                        .setLabel('Disconnect')
                        .setStyle('DANGER'),
                    new MessageButton()
                        .setCustomId('buttoncontrol_queue')
                        .setLabel('Show queue')
                        .setStyle('SECONDARY')
                )

                let status: string;
                if(!isPaused){
                    Queue.setPaused(true);
                    status = "paused";
                }else{
                    Queue.setPaused(false);
                    status = "resumed";
                }

                const title = ['spotify-custom', 'soundcloud-custom'].includes(Queue.current.source) ?
                    `${Queue.current.author} - ${Queue.current.title}` : `${Queue.current.title}`;

                channel.lastMessage?.edit({
                    embeds: [
                        {
                            title: `Now playing`,
                            description: `**[${title}](${Queue.current.url})** - ${Queue.current.requestedBy}\n\n${status} by ${interaction.user}`,
                            thumbnail: {
                                url: `${Queue.current.thumbnail}`
                            },
                            color: isPaused ? 0x44b868 : 0xb84e44,
                        }
                    ],
                    components: [row]
                });
                interaction.deferUpdate({ fetchReply: false });
                break;
            }

            case 'buttoncontrol_skip': {
                await interaction.deferReply();
                Queue.skip();
                await interaction.editReply('Skipped');
                break;
            }

            case 'buttoncontrol_disconnect': {
                await interaction.deferReply();
                Queue.destroy(true);
                await interaction.editReply('Disconnected from the voice channel.');
                break;
            }

            case 'buttoncontrol_queue': {
                const page = 1;
                const pageStart = 10 * (page - 1);
                const pageEnd = pageStart + 10;
                const currentTrack = Queue.current;
                const tracks = Queue.tracks.slice(pageStart, pageEnd).map((m, i) => {
                    return `${i + pageStart + 1}. **${m.title}** ([link](${m.url}))`;
                });
        
                const msg = new MessageEmbed();
                msg.setColor("#0099ff");
                msg.setTitle(`:play_pause: Music | Queue`);
                msg.setDescription(tracks.join("\n"));
                msg.addField("Current Track", `**${currentTrack.title}** ([link](${currentTrack.url}))`);
                msg.setFooter({ text: `© 2022 by ${process.env.BOT_AUTHOR}` });
        
                channel.lastMessage?.edit({ embeds: [ msg ] });
                interaction.deferUpdate({ fetchReply: false });
                break;
            }
        }
    }

    // Menu
    if(interaction.isSelectMenu()){

    }
};
