"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onTrackEnd = void 0;
const onTrackEnd = async (Queue, track) => {
    if (Queue.npmessage) {
        Queue.npmessage.delete().catch(() => { });
    }
};
exports.onTrackEnd = onTrackEnd;
