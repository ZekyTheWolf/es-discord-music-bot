"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onTrackStart = void 0;
const discord_js_1 = require("discord.js");
const Utils_1 = require("../../Utils");
const onTrackStart = async (Queue, track) => {
    const channel = Queue.metadata;
    if (Queue.npmessage) {
        Queue.npmessage.delete().catch(() => { });
    }
    let row = new discord_js_1.MessageActionRow()
        .addComponents(new discord_js_1.MessageButton()
        .setCustomId('buttoncontrol_play')
        .setLabel('Pause')
        .setStyle('SUCCESS'), new discord_js_1.MessageButton()
        .setCustomId('buttoncontrol_skip')
        .setLabel('Skip')
        .setStyle('PRIMARY'), new discord_js_1.MessageButton()
        .setCustomId('buttoncontrol_disconnect')
        .setLabel('Disconnect')
        .setStyle('DANGER'), new discord_js_1.MessageButton()
        .setCustomId('buttoncontrol_queue')
        .setLabel('Show queue')
        .setStyle('SECONDARY'));
    const msgEmbed = (0, Utils_1.MakeEmbed)({
        title: `:play_pause: Music | Now playing`,
        color: `#00ff00`,
        description: `Now playing ${track.title}.`,
        thumbnail: `${track.thumbnail}`,
    });
    channel.send({
        embeds: [msgEmbed],
        components: [row]
    }).then((msg) => {
        Queue.npmessage = msg;
    });
};
exports.onTrackStart = onTrackStart;
