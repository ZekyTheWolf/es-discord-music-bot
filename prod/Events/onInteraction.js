"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onInteraction = void 0;
const discord_js_1 = require("discord.js");
const _CommandList_1 = require("../Commands/_CommandList");
const Utils_1 = require("../Utils");
const __1 = require("..");
const onInteraction = async (interaction) => {
    var _a, _b, _c, _d, _e, _f;
    const guild = interaction.guild;
    // Slash commands
    if (interaction.isCommand()) {
        if ((_a = interaction.member) === null || _a === void 0 ? void 0 : _a.user.bot)
            return;
        for (const Command of _CommandList_1.CommandList) {
            if (interaction.commandName === Command.data.name) {
                if (Command.permissions == Utils_1.Permissions.NONE) {
                    await Command.run(interaction);
                }
                else {
                    if ((_b = interaction.memberPermissions) === null || _b === void 0 ? void 0 : _b.has(Command.permissions)) {
                        await Command.run(interaction);
                    }
                    else {
                        const msg = (0, Utils_1.MakeEmbed)({
                            title: ":x: | You don't have permission to use this command!",
                            color: "#ff0000",
                            description: `You need the permission \`${Command.permissions}\` to use this command.`
                        });
                        interaction.reply({ embeds: [msg] });
                    }
                }
            }
        }
    }
    // Music Player
    if (interaction.isButton()) {
        const Queue = __1.MusicPlayer.getQueue(guild.id);
        const isPaused = Queue.connection.paused;
        const channel = Queue.metadata;
        const member = interaction.member;
        const voiceChannel = member.voice.channel;
        if (!Queue
            || !Queue.playing
            || !voiceChannel.id
            || (((_c = guild.me) === null || _c === void 0 ? void 0 : _c.voice.channelId) && member.voice.channelId !== ((_d = guild.me) === null || _d === void 0 ? void 0 : _d.voice.channelId)))
            return;
        switch (interaction.customId) {
            case 'buttoncontrol_play': {
                let row = new discord_js_1.MessageActionRow()
                    .addComponents(new discord_js_1.MessageButton()
                    .setCustomId('buttoncontrol_play')
                    .setLabel(isPaused ? 'Pause' : 'Resume')
                    .setStyle('SUCCESS'), new discord_js_1.MessageButton()
                    .setCustomId('buttoncontrol_skip')
                    .setLabel('Skip')
                    .setStyle('PRIMARY'), new discord_js_1.MessageButton()
                    .setCustomId('buttoncontrol_disconnect')
                    .setLabel('Disconnect')
                    .setStyle('DANGER'), new discord_js_1.MessageButton()
                    .setCustomId('buttoncontrol_queue')
                    .setLabel('Show queue')
                    .setStyle('SECONDARY'));
                let status;
                if (!isPaused) {
                    Queue.setPaused(true);
                    status = "paused";
                }
                else {
                    Queue.setPaused(false);
                    status = "resumed";
                }
                const title = ['spotify-custom', 'soundcloud-custom'].includes(Queue.current.source) ?
                    `${Queue.current.author} - ${Queue.current.title}` : `${Queue.current.title}`;
                (_e = channel.lastMessage) === null || _e === void 0 ? void 0 : _e.edit({
                    embeds: [
                        {
                            title: `Now playing`,
                            description: `**[${title}](${Queue.current.url})** - ${Queue.current.requestedBy}\n\n${status} by ${interaction.user}`,
                            thumbnail: {
                                url: `${Queue.current.thumbnail}`
                            },
                            color: isPaused ? 0x44b868 : 0xb84e44,
                        }
                    ],
                    components: [row]
                });
                interaction.deferUpdate({ fetchReply: false });
                break;
            }
            case 'buttoncontrol_skip': {
                await interaction.deferReply();
                Queue.skip();
                await interaction.editReply('Skipped');
                break;
            }
            case 'buttoncontrol_disconnect': {
                await interaction.deferReply();
                Queue.destroy(true);
                await interaction.editReply('Disconnected from the voice channel.');
                break;
            }
            case 'buttoncontrol_queue': {
                const page = 1;
                const pageStart = 10 * (page - 1);
                const pageEnd = pageStart + 10;
                const currentTrack = Queue.current;
                const tracks = Queue.tracks.slice(pageStart, pageEnd).map((m, i) => {
                    return `${i + pageStart + 1}. **${m.title}** ([link](${m.url}))`;
                });
                const msg = new discord_js_1.MessageEmbed();
                msg.setColor("#0099ff");
                msg.setTitle(`:play_pause: Music | Queue`);
                msg.setDescription(tracks.join("\n"));
                msg.addField("Current Track", `**${currentTrack.title}** ([link](${currentTrack.url}))`);
                msg.setFooter({ text: `© 2022 by ${process.env.BOT_AUTHOR}` });
                (_f = channel.lastMessage) === null || _f === void 0 ? void 0 : _f.edit({ embeds: [msg] });
                interaction.deferUpdate({ fetchReply: false });
                break;
            }
        }
    }
    // Menu
    if (interaction.isSelectMenu()) {
    }
};
exports.onInteraction = onInteraction;
