"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.onReady = void 0;
const rest_1 = require("@discordjs/rest");
const v9_1 = require("discord-api-types/v9");
const cli_color_1 = __importDefault(require("cli-color"));
const _CommandList_1 = require("../Commands/_CommandList");
const onReady = async (BOT) => {
    var _a, _b;
    const rest = new rest_1.REST({ version: "9" }).setToken(process.env.BOT_TOKEN);
    const commandData = _CommandList_1.CommandList.map((command) => command.data.toJSON());
    await rest.put(v9_1.Routes.applicationCommands(((_a = BOT.user) === null || _a === void 0 ? void 0 : _a.id) || "missing id"), { body: commandData });
    console.log(cli_color_1.default.yellow(`| -------------------------------------`));
    console.log(cli_color_1.default.yellow(`| ${cli_color_1.default.green(`Starting EmperorBOT core!`)}`));
    console.log(cli_color_1.default.yellow(`| ${cli_color_1.default.green(`Logged as: ${(_b = BOT.user) === null || _b === void 0 ? void 0 : _b.tag}`)}`));
};
exports.onReady = onReady;
