"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueueInterface = exports.CommandCategory = void 0;
var Command_1 = require("./Command");
Object.defineProperty(exports, "CommandCategory", { enumerable: true, get: function () { return Command_1.CommandCategory; } });
var Queue_1 = require("./Music/Queue");
Object.defineProperty(exports, "QueueInterface", { enumerable: true, get: function () { return Queue_1.QueueInterface; } });
