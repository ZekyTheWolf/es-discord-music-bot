"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandCategory = void 0;
class CommandCategory {
    static get Moderation() {
        return "Moderation";
    }
    static get Fun() {
        return "Fun";
    }
    static get Music() {
        return "Music";
    }
    static get Utility() {
        return "Utility";
    }
    static get Test() {
        return "Test";
    }
    static get Dev() {
        return "Dev";
    }
    static get Info() {
        return 'Info';
    }
}
exports.CommandCategory = CommandCategory;
