"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Info = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
exports.Info = {
    name: "Info Command",
    category: Interfaces_1.CommandCategory.Info,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("info")
        .setDescription("Info about server."),
    run: async (interaction) => {
        await interaction.deferReply();
        const msg = (0, Utils_1.MakeEmbed)({
            title: `Bot info`,
            color: `#FF0000`,
            description: `\nBot is replacment for serveral bots, it can be assumend as All In One.`,
            fields: [
                { name: `Author`, value: `Emperor Studio [ES]`, inline: true },
                { name: `Version`, value: `${process.env.BOT_VERSION}`, inline: true },
                { name: `Status`, value: `Development`, inline: true }
            ]
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
