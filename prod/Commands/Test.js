"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Test = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../Interfaces");
const Utils_1 = require("../Utils");
exports.Test = {
    name: "Test Command",
    category: Interfaces_1.CommandCategory.Test,
    permissions: Utils_1.Permissions.ADMINISTRATOR,
    data: new builders_1.SlashCommandBuilder()
        .setName("test")
        .setDescription("Check in for the 100 Days of Code challenge."),
    run: async (interaction) => {
        interaction.reply("Hello World!");
    }
};
