"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Play = void 0;
const builders_1 = require("@discordjs/builders");
const discord_player_1 = require("discord-player");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.Play = {
    name: "Play",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("play")
        .setDescription("Play music from youtube.")
        .addStringOption(option => option.setName('track')
        .setDescription('Url to music/Name music')
        .setRequired(true)),
    run: async (interaction) => {
        await interaction.deferReply();
        const member = interaction.member;
        const songTitle = interaction.options.getString("track");
        if (!member.voice.channel)
            return void interaction.followUp({
                content: "Please join a voice channel first!",
            });
        const searchResult = await __1.MusicPlayer.search(songTitle, {
            requestedBy: interaction.user,
            searchEngine: discord_player_1.QueryType.AUTO,
        });
        if (!searchResult || !searchResult.tracks.length) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | No results found!`,
                color: `#ff0000`,
                description: `No results found for ${songTitle}.`
            });
            return void interaction.editReply({ embeds: [msg] });
        }
        const Queue = __1.MusicPlayer.createQueue(interaction.guild, {
            ytdlOptions: {
                filter: 'audioonly',
                highWaterMark: 1 << 30,
                dlChunkSize: 0,
            },
            metadata: interaction.channel,
        });
        try {
            if (!Queue.connection)
                await Queue.connect(member.voice.channel);
        }
        catch (_a) {
            void __1.MusicPlayer.deleteQueue(interaction.guild);
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Error!`,
                color: `#ff0000`,
                description: `Sorry, im unable to join channel.`
            });
            return void interaction.editReply({ embeds: [msg] });
        }
        if (searchResult.playlist) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: ":play_pause: Music | Playlist found!",
                color: "GREEN",
                description: `Playlist : ${searchResult.playlist.title} found!`
            });
            Queue.addTracks(searchResult.tracks);
            await interaction.editReply({ embeds: [msg] });
        }
        else {
            const msg = (0, Utils_1.MakeEmbed)({
                title: ":play_pause: Music | Track found!",
                color: "GREEN",
                description: `Track : ${searchResult.tracks[0].title} added!`
            });
            Queue.addTrack(searchResult.tracks[0]);
            await interaction.editReply({ embeds: [msg] });
        }
        if (!Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: ":play_pause: Music | Playing!",
                color: "GREEN",
                description: `Playing : ${searchResult.tracks[0].title}`,
                thumbnail: searchResult.tracks[0].thumbnail
            });
            await Queue.play();
            await interaction.editReply({ embeds: [msg] });
        }
    }
};
