"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Stop = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.Stop = {
    name: "Stop",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("stop")
        .setDescription("Stop current queue."),
    run: async (interaction) => {
        await interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }
        Queue.destroy();
        const msg = (0, Utils_1.MakeEmbed)({
            title: `:play_pause: Music | Success`,
            color: `#00ff00`,
            description: "Queue stopped."
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
