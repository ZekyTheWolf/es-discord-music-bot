"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Back = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.Back = {
    name: "Back",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("back")
        .setDescription("Start playing previouse song."),
    run: async (interaction) => {
        await interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            return void interaction.followUp({ embeds: [msg] });
        }
        await Queue.back();
        const msg = (0, Utils_1.MakeEmbed)({
            title: `:white_check_mark: Music | Back!`,
            color: `#00ff00`,
            description: `Now playing ${Queue.current.title}`
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
