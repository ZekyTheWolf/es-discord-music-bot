"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BassBoost = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.BassBoost = {
    name: "bassboost",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("bassboost")
        .setDescription("Toggle bass boost filter."),
    run: async (interaction) => {
        await interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            return void interaction.followUp({ embeds: [msg] });
        }
        await Queue.setFilters({
            bassboost: !Queue.getFiltersEnabled().includes('bassboost'),
            normalizer2: !Queue.getFiltersEnabled().includes('bassboost') // because we need to toggle it with bass
        });
        const msg = (0, Utils_1.MakeEmbed)({
            title: `:white_check_mark: Music | Bass boost`,
            color: `#00ff00`,
            description: `Bass boost filter is now ${Queue.getFiltersEnabled().includes('bassboost') ? 'enabled' : 'disabled'}`
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
