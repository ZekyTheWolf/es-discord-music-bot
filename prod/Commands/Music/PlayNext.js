"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlayNext = void 0;
const builders_1 = require("@discordjs/builders");
const discord_player_1 = require("discord-player");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.PlayNext = {
    name: "Next",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("playnext")
        .setDescription("Track what you want play next.")
        .addStringOption(option => option.setName('track')
        .setDescription('Url to music/Name music')
        .setRequired(true)),
    run: async (interaction) => {
        interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        const Track = interaction.options.getString('track');
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }
        const searchResult = await __1.MusicPlayer
            .search(Track, {
            requestedBy: interaction.user,
            searchEngine: discord_player_1.QueryType.AUTO
        })
            .catch(() => {
            console.log('error search');
        });
        if (!searchResult) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: ":x: Music | Error!",
                color: "#ff0000",
                description: `Something went wrong, check console.`
            });
            return void interaction.editReply({ embeds: [msg] });
        }
        Queue.insert(searchResult.tracks[0]);
        const msg = (0, Utils_1.MakeEmbed)({
            title: ":play_pause: Music",
            color: "GREEN",
            description: `Next track will be ${searchResult.tracks[0].title}`
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
