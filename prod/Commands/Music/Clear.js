"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MusicClear = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.MusicClear = {
    name: "Clear",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("queue-clear")
        .setDescription("Clear current queue."),
    run: async (interaction) => {
        await interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }
        Queue.clear();
        const msg = (0, Utils_1.MakeEmbed)({
            title: `:white_check_mark: Music | Queue cleared!`,
            color: `#00ff00`,
            description: `Queue cleared.`
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
