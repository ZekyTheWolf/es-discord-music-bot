"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentQueue = void 0;
const discord_js_1 = require("discord.js");
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.CurrentQueue = {
    name: "Queue",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("queue")
        .setDescription("Check in for the 100 Days of Code challenge.")
        .addIntegerOption(option => option.setName('page')
        .setDescription('Specific page of the queue.')
        .setRequired(false)),
    run: async (interaction) => {
        await interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }
        const page = interaction.options.getInteger('page') || 1;
        const pageStart = 10 * (page - 1);
        const pageEnd = pageStart + 10;
        const currentTrack = Queue.current;
        const tracks = Queue.tracks.slice(pageStart, pageEnd).map((m, i) => {
            return `${i + pageStart + 1}. **${m.title}** ([link](${m.url}))`;
        });
        const msg = new discord_js_1.MessageEmbed();
        msg.setColor("#0099ff");
        msg.setTitle(`:play_pause: Music | Queue`);
        msg.setDescription(tracks.join("\n"));
        msg.addField("Current Track", `**${currentTrack.title}** ([link](${currentTrack.url}))`);
        msg.setFooter({ text: `© 2022 by ${process.env.BOT_AUTHOR}` });
        await interaction.editReply({ embeds: [msg] });
    }
};
