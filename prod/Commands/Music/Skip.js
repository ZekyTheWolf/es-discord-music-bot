"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Skip = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.Skip = {
    name: "Skip",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("skip")
        .setDescription("Skip to next track."),
    run: async (interaction) => {
        await interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }
        const success = Queue.skip();
        const currentTrack = Queue.current;
        const msg = (0, Utils_1.MakeEmbed)({
            title: success ? `:play_pause: Music | Skipped` : `:x: Music | Error!`,
            color: success ? `#00ff00` : `#ff0000`,
            description: success ? `Skipped to ${currentTrack.title}.` : "Error."
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
