"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pause = void 0;
const builders_1 = require("@discordjs/builders");
const Interfaces_1 = require("../../Interfaces");
const Utils_1 = require("../../Utils");
const __1 = require("../..");
exports.Pause = {
    name: "Pause",
    category: Interfaces_1.CommandCategory.Music,
    permissions: Utils_1.Permissions.NONE,
    data: new builders_1.SlashCommandBuilder()
        .setName("pause")
        .setDescription("Pause current track."),
    run: async (interaction) => {
        await interaction.deferReply();
        const Queue = __1.MusicPlayer.getQueue(interaction.guildId);
        if (!Queue || !Queue.playing) {
            const msg = (0, Utils_1.MakeEmbed)({
                title: `:x: Music | Nothing to play!`,
                color: `#ff0000`,
                description: `There is nothing to play.`
            });
            await interaction.followUp({ embeds: [msg] });
        }
        const paused = Queue.setPaused(true);
        const msg = (0, Utils_1.MakeEmbed)({
            title: paused ? `:pause_button: Music | Paused!` : `:x: Music | Error!`,
            color: paused ? `#00ff00` : `#ff0000`,
            description: paused ? "Paused." : "Error."
        });
        await interaction.editReply({ embeds: [msg] });
    }
};
