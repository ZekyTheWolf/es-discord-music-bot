"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BOT = exports.MusicPlayer = void 0;
const Bot_1 = require("./Bot");
Object.defineProperty(exports, "BOT", { enumerable: true, get: function () { return Bot_1.BOT; } });
Object.defineProperty(exports, "MusicPlayer", { enumerable: true, get: function () { return Bot_1.MusicPlayer; } });
(0, Bot_1.startBot)();
