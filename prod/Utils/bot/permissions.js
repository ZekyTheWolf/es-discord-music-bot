"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Permissions = void 0;
class Permissions {
    static get CREATE_INSTANT_INVITE() {
        return "CREATE_INSTANT_INVITE";
    }
    static get KICK_MEMBERS() {
        return "KICK_MEMBERS";
    }
    static get BAN_MEMBERS() {
        return "BAN_MEMBERS";
    }
    static get ADMINISTRATOR() {
        return "ADMINISTRATOR";
    }
    static get MANAGE_CHANNELS() {
        return "MANAGE_CHANNELS";
    }
    static get MANAGE_GUILD() {
        return "MANAGE_GUILD";
    }
    static get ADD_REACTIONS() {
        return "ADD_REACTIONS";
    }
    static get VIEW_AUDIT_LOG() {
        return "VIEW_AUDIT_LOG";
    }
    static get VIEW_CHANNEL() {
        return "VIEW_CHANNEL";
    }
    static get SEND_MESSAGES() {
        return "SEND_MESSAGES";
    }
    static get SEND_TTS_MESSAGES() {
        return "SEND_TTS_MESSAGES";
    }
    static get MANAGE_MESSAGES() {
        return "MANAGE_MESSAGES";
    }
    static get EMBED_LINKS() {
        return "EMBED_LINKS";
    }
    static get ATTACH_FILES() {
        return "ATTACH_FILES";
    }
    static get READ_MESSAGE_HISTORY() {
        return "READ_MESSAGE_HISTORY";
    }
    static get MENTION_EVERYONE() {
        return "MENTION_EVERYONE";
    }
    static get USE_EXTERNAL_EMOJIS() {
        return "USE_EXTERNAL_EMOJIS";
    }
    static get CONNECT() {
        return "CONNECT";
    }
    static get SPEAK() {
        return "SPEAK";
    }
    static get MUTE_MEMBERS() {
        return "MUTE_MEMBERS";
    }
    static get DEAFEN_MEMBERS() {
        return "DEAFEN_MEMBERS";
    }
    static get MOVE_MEMBERS() {
        return "MOVE_MEMBERS";
    }
    static get USE_VAD() {
        return "USE_VAD";
    }
    static get CHANGE_NICKNAME() {
        return "CHANGE_NICKNAME";
    }
    static get MANAGE_NICKNAMES() {
        return "MANAGE_NICKNAMES";
    }
    static get MANAGE_ROLES() {
        return "MANAGE_ROLES";
    }
    static get MANAGE_WEBHOOKS() {
        return "MANAGE_WEBHOOKS";
    }
    static get MANAGE_EMOJIS_AND_STICKERS() {
        return "MANAGE_EMOJIS_AND_STICKERS";
    }
    static get USE_APPLICATION_COMMANDS() {
        return "USE_APPLICATION_COMMANDS";
    }
    static get REQUEST_TO_SPEAK() {
        return "REQUEST_TO_SPEAK";
    }
    static get MANAGE_THREADS() {
        return "MANAGE_THREADS";
    }
    static get USE_PUBLIC_THREADS() {
        return "USE_PUBLIC_THREADS";
    }
    static get CREATE_PUBLIC_THREADS() {
        return "CREATE_PUBLIC_THREADS";
    }
    static get CREATE_PRIVATE_THREADS() {
        return "CREATE_PRIVATE_THREADS";
    }
    static get USE_EXTERNAL_STICKERS() {
        return "USE_EXTERNAL_STICKERS";
    }
    static get SEND_MESSAGES_IN_THREADS() {
        return "SEND_MESSAGES_IN_THREADS";
    }
    static get START_EMBEDDED_ACTIVITIES() {
        return "START_EMBEDDED_ACTIVITIES";
    }
    static get MODERATE_MEMBERS() {
        return "MODERATE_MEMBERS";
    }
    static get MANAGE_EVENTS() {
        return "MANAGE_EVENTS";
    }
    static get NONE() {
        return "NONE";
    }
}
exports.Permissions = Permissions;
