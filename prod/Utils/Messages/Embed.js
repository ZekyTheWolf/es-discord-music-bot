"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MakeEmbed = void 0;
const discord_js_1 = require("discord.js");
function MakeEmbed(embed) {
    let msg = new discord_js_1.MessageEmbed();
    msg.setColor(embed.color);
    msg.setTitle(embed.title);
    msg.setDescription(embed.description);
    if (embed.thumbnail)
        msg.setThumbnail(embed.thumbnail);
    if (embed.fields)
        msg.addFields(embed.fields);
    msg.setFooter({ text: `© 2022 by ${process.env.BOT_AUTHOR}` });
    return msg;
}
exports.MakeEmbed = MakeEmbed;
