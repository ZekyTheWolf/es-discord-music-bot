"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerType = exports.LoggerPart = exports.Logger = void 0;
const cli_color_1 = __importDefault(require("cli-color"));
const log4js = __importStar(require("log4js"));
// Logger(user, danger, `Cant update user ${this.user.id}`);
const Logger = (part, type, message, file = false, fileName = "") => {
    let loggerPart;
    switch (part) {
        case LoggerPart.database:
            {
                loggerPart = "Database";
                break;
            }
            ;
        case LoggerPart.user:
            {
                loggerPart = "User";
                break;
            }
            ;
        case LoggerPart.levelSystem:
            {
                loggerPart = "LevelSystem";
                break;
            }
            ;
        case LoggerPart.commands:
            {
                loggerPart = "Commands";
                break;
            }
            ;
    }
    let loggerType;
    switch (type) {
        case LoggerType.success:
            {
                loggerType = cli_color_1.default.greenBright;
                break;
            }
            ;
        case LoggerType.danger:
            {
                loggerType = cli_color_1.default.redBright;
                break;
            }
            ;
        case LoggerType.warning:
            {
                loggerType = cli_color_1.default.yellowBright;
                break;
            }
            ;
        case LoggerType.info:
            {
                loggerType = cli_color_1.default.cyanBright;
                break;
            }
            ;
    }
    console.log(loggerType(`| ------------------------------------- |`));
    console.log(loggerType(`| -> ${loggerType(loggerPart)}`));
    console.log(loggerType(`| -> ${cli_color_1.default.white(message)}`));
    console.log(loggerType(`| ------------------------------------- |`));
    if (file) {
        log4js.configure({
            appenders: { cheese: { type: "file", filename: fileName ? fileName : "logs/log.log" } },
            categories: { default: { appenders: ["ES"], level: "error" } }
        });
        const logger = log4js.getLogger("ES");
        switch (type) {
            case LoggerType.success:
                {
                    logger.info(`${message}`);
                    break;
                }
                ;
            case LoggerType.danger:
                {
                    logger.error(`${message}`);
                    break;
                }
                ;
            case LoggerType.warning:
                {
                    logger.warn(`${message}`);
                    break;
                }
                ;
            case LoggerType.info:
                {
                    logger.debug(`${message}`);
                    break;
                }
                ;
        }
    }
};
exports.Logger = Logger;
var LoggerPart;
(function (LoggerPart) {
    LoggerPart[LoggerPart["database"] = 0] = "database";
    LoggerPart[LoggerPart["user"] = 1] = "user";
    LoggerPart[LoggerPart["levelSystem"] = 2] = "levelSystem";
    LoggerPart[LoggerPart["commands"] = 3] = "commands";
})(LoggerPart = exports.LoggerPart || (exports.LoggerPart = {}));
var LoggerType;
(function (LoggerType) {
    LoggerType[LoggerType["success"] = 0] = "success";
    LoggerType[LoggerType["warning"] = 1] = "warning";
    LoggerType[LoggerType["danger"] = 2] = "danger";
    LoggerType[LoggerType["info"] = 3] = "info";
})(LoggerType = exports.LoggerType || (exports.LoggerType = {}));
